<?php
/**
 *      __                _                 ___                           _   _
 *   /\ \ \__ _ _ __ ___ (_)_ __   __ _    / __\___  _ ____   _____ _ __ | |_(_) ___  _ __  ___
 *  /  \/ / _` | '_ ` _ \| | '_ \ / _` |  / /  / _ \| '_ \ \ / / _ \ '_ \| __| |/ _ \| '_ \/ __|
 * / /\  / (_| | | | | | | | | | | (_| | / /__| (_) | | | \ V /  __/ | | | |_| | (_) | | | \__ \
 * \_\ \/ \__,_|_| |_| |_|_|_| |_|\__, | \____/\___/|_| |_|\_/ \___|_| |_|\__|_|\___/|_| |_|___/
 *                                |___/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu;

/**
 * Class NamingConventions
 * @package Rodziu
 */
abstract class NamingConventions{
	/**
	 * @var string
	 */
	public static $encoding = 'UTF-8';

	/**
	 * @param string $char
	 *
	 * @return string
	 */
	protected static function getType(string $char): string{
		if((string)(int)$char == $char){
			return 'number';
		}else if(preg_match('#[a-z]#u', $char)){
			return 'lowercase';
		}else if(preg_match('#[A-Z]#u', $char)){
			return 'uppercase';
		}
		return 'special';
	}

	/**
	 * @param string $string
	 *
	 * @return array
	 */
	protected static function parse(string $string): array{
		$len = mb_strlen($string, self::$encoding);
		$ret = [];
		$prevType = $stack = '';
		$compareTypes = function($prevType, $type, $stack): bool{
			if($type == $prevType){
				return true;
			}else if($type == 'number'){
				return $prevType != 'special';
			}else if($type == 'lowercase' && $prevType == 'uppercase'){
				return mb_strlen($stack, self::$encoding) == 1; // for camelCase
			}
			return false;
		};
		for($i = 0; $i < $len; $i++){
			$type = self::getType($string[$i]);
			if($i > 0 && !$compareTypes($prevType, $type, $stack) && $stack != ''){
				$ret[] = $stack;
				$stack = '';
			}
			if($i == 0 || $type != 'special'){
				$stack .= mb_strtolower($string[$i], self::$encoding);
			}
			//
			$prevType = $type;
		}
		if($stack != ''){
			$ret[] = $stack;
		}
		return $ret;
	}

	/**
	 * Converts string to camelCase
	 *
	 * @param string $input
	 *
	 * @return string
	 */
	public static function toCamelCase(string $input): string{
		$parsed = self::parse($input);
		foreach($parsed as $k => &$p){
			if($k > 0){
				$len = mb_strlen($p, self::$encoding);
				$p = mb_strtoupper(mb_substr($p, 0, 1, self::$encoding), self::$encoding)
					.mb_substr($p, 1, $len - 1, self::$encoding);
			}
		}
		unset($p);
		return implode('', $parsed);
	}

	/**
	 * Converts string to PascalCase
	 *
	 * @param string $input
	 *
	 * @return string
	 */
	public static function toPascalCase(string $input): string{
		$parsed = self::parse($input);
		foreach($parsed as &$p){
			$len = mb_strlen($p, self::$encoding);
			$p = mb_strtoupper(mb_substr($p, 0, 1, self::$encoding), self::$encoding)
				.mb_substr($p, 1, $len - 1, self::$encoding);
		}
		unset($p);
		return implode('', $parsed);
	}

	/**
	 * Converts string to snake_case
	 *
	 * @param string $input
	 *
	 * @return string
	 */
	public static function toSnakeCase(string $input): string{
		return implode('_', self::parse($input));
	}

	/**
	 * Converts string to kebab-case
	 *
	 * @param string $input
	 *
	 * @return string
	 */
	public static function toKebabCase(string $input): string{
		return implode('-', self::parse($input));
	}
}