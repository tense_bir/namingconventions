<?php
/**
 *      __                _                 ___                           _   _
 *   /\ \ \__ _ _ __ ___ (_)_ __   __ _    / __\___  _ ____   _____ _ __ | |_(_) ___  _ __  ___
 *  /  \/ / _` | '_ ` _ \| | '_ \ / _` |  / /  / _ \| '_ \ \ / / _ \ '_ \| __| |/ _ \| '_ \/ __|
 * / /\  / (_| | | | | | | | | | | (_| | / /__| (_) | | | \ V /  __/ | | | |_| | (_) | | | \__ \
 * \_\ \/ \__,_|_| |_| |_|_|_| |_|\__, | \____/\___/|_| |_|\_/ \___|_| |_|\__|_|\___/|_| |_|___/
 *                                |___/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu;

use \PHPUnit\Framework\TestCase;

/**
 * Class NamingConventionsTest
 */
class NamingConventionsTest extends TestCase{
	/**
	 * @dataProvider parseProvider
	 *
	 * @param string $string
	 * @param array $output
	 */
	public function testParse(string $string, array $output){
		$mock = new class extends NamingConventions{
			public function parseStub(string $string): array{
				return self::parse($string);
			}
		};
		$this->assertEquals($output, $mock->parseStub($string));
	}

	/**
	 * @return array
	 */
	public function parseProvider(): array{
		$strings = [
			['', []],
			['a', ['a']],
			['some-string', ['some', 'string']],
			['someString', ['some', 'string']],
			['some-mixed_conventions___StringSTRING', ['some', 'mixed', 'conventions', 'string', 'string']],
			['StringWith124Numbers', ['string', 'with124', 'numbers']],
			['string-with%^specialCharacters#', ['string', 'with', 'special', 'characters']]
		];
		$ret = [];
		foreach($strings as $s){
			$ret[$s[0]] = $s;
		}
		return $ret;
	}

	/**
	 * @param array $outputs
	 *
	 * @return array
	 */
	private function mergeProvider(array $outputs): array{
		$ret = $this->parseProvider();
		$i = 0;
		foreach($ret as &$r){
			$r[1] = $outputs[$i++];
		}
		unset($r);
		return $ret;
	}

	/**
	 * @dataProvider camelCaseProvider
	 * @param string $input
	 * @param string $output
	 */
	public function testToCamelCase(string $input, string $output){
		$this->assertEquals($output, NamingConventions::toCamelCase($input));
	}

	/**
	 * @return array
	 */
	public function camelCaseProvider(): array{
		return $this->mergeProvider([
			'', 'a', 'someString', 'someString', 'someMixedConventionsStringString', 'stringWith124Numbers', 'stringWithSpecialCharacters'
		]);
	}

	/**
	 * @dataProvider PascalCaseProvider
	 * @param string $input
	 * @param string $output
	 */
	public function testToPascalCase(string $input, string $output){
		$this->assertEquals($output, NamingConventions::toPascalCase($input));
	}

	/**
	 * @return array
	 */
	public function PascalCaseProvider(): array{
		return $this->mergeProvider([
			'', 'A', 'SomeString', 'SomeString', 'SomeMixedConventionsStringString', 'StringWith124Numbers', 'StringWithSpecialCharacters'
		]);
	}

	/**
	 * @dataProvider snakeCaseProvider
	 * @param string $input
	 * @param string $output
	 */
	public function testToSnakeCase(string $input, string $output){
		$this->assertEquals($output, NamingConventions::toSnakeCase($input));
	}

	/**
	 * @return array
	 */
	public function snakeCaseProvider(): array{
		return $this->mergeProvider([
			'', 'a', 'some_string', 'some_string', 'some_mixed_conventions_string_string', 'string_with124_numbers', 'string_with_special_characters'
		]);
	}

	/**
	 * @dataProvider kebabCaseProvider
	 * @param string $input
	 * @param string $output
	 */
	public function testToKebabCase(string $input, string $output){
		$this->assertEquals($output, NamingConventions::toKebabCase($input));
	}

	/**
	 * @return array
	 */
	public function kebabCaseProvider(): array{
		return $this->mergeProvider([
			'', 'a', 'some-string', 'some-string', 'some-mixed-conventions-string-string', 'string-with124-numbers', 'string-with-special-characters'
		]);
	}
}
